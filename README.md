# [ma_124.gitlab.io/color-comparator][Demo]
[Try it.][Demo] ([Only Dark][DemoDark] / [Only Light][DemoLight])

A color scheme previewer for themes based on [Base16](https://github.com/chriskempson/base16).

- [`index.html`](./index.html): The page source code.
- [`collect-themes.js`](./collect-themes.js): Generate a `themes.json` from YAML files.
- [Default `themes.json`](https://gitlab.com/-/snippets/2179322/raw/main/themes.json): The themes in the demo version.

[Demo]: https://ma_124.gitlab.io/color-comparator
[DemoDark]: https://ma_124.gitlab.io/color-comparator/dark.html
[DemoLight]: https://ma_124.gitlab.io/color-comparator/light.html
