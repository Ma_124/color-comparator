#!/usr/bin/node

const fs = require("fs");
const path = require("path");
const process = require("process");

async function* walk(dir) {
    for await (const d of await fs.promises.opendir(dir)) {
        const entry = path.join(dir, d.name);
        if (d.isDirectory()) yield* walk(entry);
        else if (d.isFile()) yield entry;
    }
}

async function main() {
    if (process.argv.length <= 2 || process.argv[2] == "--help") {
        console.log("usage: ./collect-themes.js <dir tree with themes>")
        return;
    }

    let themes = {};

    for await (const path of walk(process.argv[2])) {
        if (!path.endsWith(".yaml") && !path.endsWith(".yml"))
            continue;

        const yaml = fs.readFileSync(path, "UTF-8");
        let theme = {};
        let theme_name = null;
        for (const line of yaml.split(/\r|\n/)) {
            let fields = line.trim().split("#", 1)[0].split(":");
            if (fields[0] == "")
                continue;

            let key = fields[0].trim().replace(/(^")|("$)/g, "");
            let value = fields.slice(1).join(":").trim().replace(/(^")|("$)/g, "");

            switch (key) {
                case "scheme":
                    theme_name = value;
                    break;
                case "author":
                case "base00":
                case "base01":
                case "base02":
                case "base03":
                case "base04":
                case "base05":
                case "base06":
                case "base07":
                case "base08":
                case "base09":
                case "base0A":
                case "base0B":
                case "base0C":
                case "base0D":
                case "base0E":
                case "base0F":
                    theme[key] = value;
                    break;
                default:
                    continue;
            }
        }

        if (theme_name != null)
            themes[theme_name] = theme;
    }

    console.log(JSON.stringify(themes));
}

main()